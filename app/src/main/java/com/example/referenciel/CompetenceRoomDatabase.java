package com.example.referenciel;



import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Competence.class}, version = 1 ,exportSchema = false)
public abstract class CompetenceRoomDatabase extends RoomDatabase {
    public abstract CompetenceDao competenceDao();
     private static volatile CompetenceRoomDatabase INSTANCE;
     static CompetenceRoomDatabase getDatabase(final Context context){
         if (INSTANCE == null){
             synchronized (CompetenceRoomDatabase.class){
                 if (INSTANCE == null){
                     INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                             CompetenceRoomDatabase.class,"competence_databas").build();
                 }
             }
         }
         return INSTANCE;
     }




}
