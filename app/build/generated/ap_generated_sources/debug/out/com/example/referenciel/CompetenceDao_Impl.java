package com.example.referenciel;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Exception;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@SuppressWarnings({"unchecked", "deprecation"})
public final class CompetenceDao_Impl implements CompetenceDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfCompetence;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAll;

  public CompetenceDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfCompetence = new EntityInsertionAdapter<Competence>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `competecnce_table`(`nomCompetence`) VALUES (?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Competence value) {
        if (value.getNomCompetence() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getNomCompetence());
        }
      }
    };
    this.__preparedStmtOfDeleteAll = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM competecnce_table";
        return _query;
      }
    };
  }

  @Override
  public void insert(final Competence competence) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfCompetence.insert(competence);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteAll() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAll.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAll.release(_stmt);
    }
  }

  @Override
  public LiveData<List<Competence>> getToutesCompetences() {
    final String _sql = "SELECT * from competecnce_table ORDER BY nomCompetence ASC";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"competecnce_table"}, false, new Callable<List<Competence>>() {
      @Override
      public List<Competence> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfNomCompetence = CursorUtil.getColumnIndexOrThrow(_cursor, "nomCompetence");
          final List<Competence> _result = new ArrayList<Competence>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Competence _item;
            final String _tmpNomCompetence;
            _tmpNomCompetence = _cursor.getString(_cursorIndexOfNomCompetence);
            _item = new Competence(_tmpNomCompetence);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }
}
